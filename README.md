# LandisPlusGyr Database

This is the database for the Landis+Gyr project. This database is based on [Timescale](https://www.timescale.com/). 

##### How to run
In order to run this database you will need [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/).
Change the [docker-compose.yml](https://gitlab.com/landisplusgyr/landisplusgyr-database/-/blob/main/docker-compose.yml)
according to your own requirements. At least change the following parameters to something
more suitable:
- POSTGRES_PASSWORD: password used for the default PostgreSQL user (postgres)
- LANDISPLUSGYR_DB_PASSWORD: password used for the landisplusgyr user 
- Ports: The database is exposed on port 5432. This port is mapped to a port on your host. I set this to 5435.
- Volumes: A directory on the host system will be mounted as a volume  to store the postgresql data (/var/lib/postgresql). 
  I set this to /var/lpg-data/postgresql. 

Run the database using the following command:
`docker-compose -f docker-compose.yml up -d`

